package com.itau.maratona;

import java.util.List;

public class App {
	public static void main (String[] args) {
		
		Arquivo arquivo = new Arquivo("/home/usuario/eclipse-workspace/03-execicio-colecoes/src/alunos.csv");
		//Arquivo arquivo = new Arquivo("c:\\home/usuario/eclipse-workspace/03-execicio-colecoes/src/alunos.csv");
		
		List<Aluno> alunos = arquivo.ler();
		
		//System.out.println(alunos);
		
		
		int tamanhoAlunos = alunos.size();
		int qtdEquipe = tamanhoAlunos / 3;
		System.out.println("Qtde de equipes: " + qtdEquipe);
		List<Equipe> equipes = CriadorEquipes.construir(qtdEquipe, alunos);
		
		Impressora.imprimir(equipes);
	}

}
