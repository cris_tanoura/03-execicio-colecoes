package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class CriadorEquipes {

	public static List<Equipe>  construir(int qtdeEquipe, List<Aluno> listaAlunos) {
		List<Equipe> listaEquipes = new ArrayList<>();
		
		for (int i = 0; i < qtdeEquipe; i++) {
			Equipe equipe = new Equipe();
			equipe.idEquipe = i;
			equipe.alunos = sortearAlunos(listaAlunos);
			listaEquipes.add(equipe);
			
		}
		
		//System.out.println(listaEquipes);
		return listaEquipes;
	}

	
	private static List<Aluno> sortearAlunos(List<Aluno> alunos) {
		List<Aluno> alunoSorteados = new ArrayList<>();
		for (int j = 0; j < 3; j++) {
			// ceil -  busca 3 elemento de array de 0 a 2 - estoura porque vai procurar o j = 3 que eh maior do que 2
			// floor - busca arredondando para baixo 
			Double valor = Math.floor(Math.random() * alunos.size());
		//	System.out.println(valor);
			alunoSorteados.add(alunos.get(valor.intValue()));
		//	System.out.println(alunos.get(valor.intValue()));
			alunos.remove(valor.intValue());
		}
	//	System.out.println(alunoSorteados);
		return alunoSorteados;
	}
}
