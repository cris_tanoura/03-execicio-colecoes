package com.itau.maratona;

import java.util.List;

public class Equipe {
	public int idEquipe;
	public List<Aluno> alunos;
	
	public String toString() {
		String equipeString = "Equipe " + idEquipe +  "\n";
		for (int i=0; i<3; i++) {
			equipeString += alunos.get(i) + "\n";
		}
	//	System.out.println(equipeString);
		return equipeString;
	}
}
